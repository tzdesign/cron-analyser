# Cronanalyser 

Special thanks to ```harrisiirak``` who made it easy with [cron-parser](https://github.com/harrisiirak/cron-parser).

Special thanks to the guys who created [ervy](https://github.com/chunqiuyiyu/ervy) for the chart implementation.




## Install

```shell
npm -g i cronanalyser
```

## Usage

### Input from crontab -l
```shell
cronanalyser
```
### File input
```shell
cronanalyser -f /path/to/your/file
```

## Modes

### Minimal
```shell
cronanalyser -m minimal
```
Returns a minified output if you have 1000 of jobs (FYI you shouldn't have 🤦‍)

### Chart
```shell
cronanalyser -m chart
```

## Output

Cronjob example:

![](/img/crontab.png)

### Normal mode

![](/img/normal.png) 

### Chart mode

![](/img/chart.png)