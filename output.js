require("colors");
const moment = require("moment");

const normal = (dates, commands) => {
  dates.forEach(d => {
    const date = d.split("|").shift();
    const next = d.split("|").pop();

    const m = moment(date);
    const n = moment(next);

    const format = m.isSame(moment(), "day") ? "HH:mm" : "LLL";
    const nextFormat = m.isSame(n, "day") ? "HH:mm" : "LLL";

    console.log(`
        time: ${m.format(format).green}
        next: ${n.format(nextFormat).yellow} \n`);

    commands[d].forEach(c => {
      const com = "\t" + c;
      console.log(com.white.trim());
    });

    console.log("\n");
  });
};

const minimal = (dates, commands) => {
  dates.forEach(d => {
    const date = moment(d.split("|").shift());
    const length = commands[d].length 
    console.log(`${date.format('LLL').green} => ${(length+' job'+(length>1?'s':'')).yellow}`)

  });
};

const chart = (dates, commands) => {
  const {bar,bg,fg} = require('ervy')
  const data = dates.reduce((r,d) => {
    const key = moment(d.split("|").shift()).format('HH:mm');
    const value = commands[d].length 

    const existing = r.findIndex(d => d.key == key);
    
    if(existing !== -1){
      r[existing].value+=value
    }
    else{
      r.push({key,value})
    }

    return r

    
  },[]);
  
  console.log(bar(data,{ style: bg('green')}));
  
};

module.exports = { normal, minimal,chart };
