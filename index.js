const parser = require("cron-parser");
const moment = require("moment");
const fs = require("fs");
const { execSync } = require("child_process");
require("moment/locale/de");
moment.locale("de");
require("colors");
const writer = require('./output')

var args = require("minimist")(process.argv.slice(2), {
  alias: { f: "file", h: "help",v:'verbose',m:'mode' },
  default:{mode:'normal'},
});


args.h && require("./help");

const reg = new RegExp(
  /([,0-9\/*\-]{1,} [,0-9\/*\-]{1,} [,0-9\/*\-]{1,} [,0-9\/*\-]{1,} [,0-9\/*\-]{1,}) (.*)/
);

let tabs = "";
if (args.file) {
  if (fs.existsSync(args.file)) {
    tabs = fs.readFileSync(args.file).toString();
  } else {
    console.log(args.file.red + " does not exits!");
    process.exit(1);
  }
} else {
  tabs = execSync("crontab -l").toLocaleString() || "";
}

const lines = tabs.split("\n");
const data = lines
  .map(l => l.trim())
  .filter(l => l !== "")
  .filter(l => /^PATH/i.test(l) === false)
  .filter(l => l.substring(0, 1) !== "#")
  .filter(l => l.substring(0, 1) !== "@")
  .filter(l => /mailto/i.test(l) === false);

const options = {
  currentDate: moment()
    .startOf("day")
    .toDate()
};

let commands = data.reduce((result, d) => {
  const [_, string, command] = d.match(reg) || [];
  
  try {
    const expression = parser.parseExpression(string, options);
    if (expression && command) {
      const date = expression.next()._date.format("YYYY-MM-DD HH:mm");
      const dateNext = expression.next()._date.format("YYYY-MM-DD HH:mm");
      const key = date + "|" + dateNext;

      if (result[key] === undefined) {
        result[key] = [];
      }

      result[key].push(command);
    }
  } catch (error) {
    args.verbose && console.log(error, string);

    return result;
  }

  return result;
}, {});

let commandCount = Object.values(commands).reduce(
  (count, coms) => count + coms.length,
  0
);
let dates = Object.keys(commands).sort();

console.log(`${commandCount} Cronjob${(commandCount>1?'s':'')}`.rainbow.white);
execSync('sleep 1')


const outputer = writer[args.m] || writer['normal']
outputer(dates,commands);
