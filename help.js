const arguments = {
  "-f / --file   ": "Reading cron from file instead of using crontab -l",
  "-m / --mode   ": "View mode values: (minimal,normal,chart)",
  "-v / --verbose": "Showing errors, because the script is buggy af!",
  '-h / --help   ':'Just help you already used it 💩',
};

console.log(
  `
____ CRONANALYSER _____`.rainbow
);
console.log(`
Cron analyser is analysing your crontab and group it by date!
  
usage:

    ${"cronanalyser".green}               reading your local crontab
    ${"cronanalyser".green +
      " --file=/path".white +
      "  for reading a specific file -f is also working"}

arguments:

${Object.entries(arguments)
  .map(e => `    ${e[0].green}             ${e[1]}`)
  .join("\n")}

  `);

process.exit(0);
